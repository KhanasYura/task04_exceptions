package com.khanas.controller;

import com.khanas.model.Dog;
import com.khanas.model.DogIsHungryException;
import com.khanas.model.DogIsNotReadyException;


import java.io.FileNotFoundException;

import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 * Controller.
 */
public class ControllerImple implements AutoCloseable, Controller {

    /**\
     * File.
     */
    private PrintWriter writer;

    /**
     * Put collar.
     * @param dog object.
     */
    public final void putCollar(final Dog dog) {

        System.out.println("Нашийник одітий!");
        dog.setCollarPutOn(true);
    }

    /**
     * Put leash.
     * @param dog object.
     */
    public final void putLeash(final Dog dog) {

        System.out.println("Поводок надітий!");
        dog.setLeashPutOn(true);
    }

    /**
     * Feed dog.
     * @param dog object.
     * @param food info about food.
     * @throws DogIsHungryException - exception.
     */
    public final void feed(final Dog dog, final double food)
            throws DogIsHungryException {

        try {
            System.out.println("Йдемо кормити собаку");
            if (food != 0) {
                System.out.println("Собака поїла");
            } else {
                dog.setHungry(true);
                throw new DogIsHungryException("Собачий корм закінчився. "
                        + dog.getName() + " є голодний");
            }
        } catch (DogIsHungryException e) {
            System.out.println(e);
        }
    }

    /**
     * Walk.
     * @param dog - object.
     * @throws DogIsNotReadyException - exception.
     */
    public final void walk(final Dog dog) throws DogIsNotReadyException {

        System.out.println("Збираємося вигулювати собаку");
        try {
            if (dog.isCollarPutOn() && dog.isLeashPutOn()) {
                System.out.println("Нашийник і поводок надягнуті");
            } else {
                throw new DogIsNotReadyException("Собака " + dog.getName()
                        + " не готова до прогулянки! "
                        + "Провірте нашийник і поводок!");
            }
        } catch (DogIsNotReadyException e) {
            System.out.println(e);
        }
    }

    /**
     * Write to file.
     * @param dog - object
     * @param food - info about food.
     * @throws FileNotFoundException - exception.
     */
    public final void writeToFile(final Dog dog, final double food)
            throws FileNotFoundException {
        try {
            writer = new PrintWriter(dog.getName() + ".txt");
            walk(dog);
            feed(dog, food);
            if (dog.isCollarPutOn() && dog.isLeashPutOn() && !dog.isHungry()) {
                writer.println("Собака " + dog.getName()
                        + " готова до прогулянки");
                writer.println();
                dog.setWalkStart(LocalDateTime.now());
                DateTimeFormatter formatter = DateTimeFormatter.
                                ofLocalizedDateTime(FormatStyle.MEDIUM);
                writer.println("Прогулянка триватиме від "
                        + dog.getWalkStart().format(formatter));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DogIsHungryException e) {
            e.printStackTrace();
        } catch (DogIsNotReadyException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void close() {
        if (writer != null) {
            System.out.println("Closing file");
            writer.close();
        }
    }
}
