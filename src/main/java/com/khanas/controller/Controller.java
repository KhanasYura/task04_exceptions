package com.khanas.controller;

import com.khanas.model.Dog;
import com.khanas.model.DogIsHungryException;
import com.khanas.model.DogIsNotReadyException;

import java.io.FileNotFoundException;

/**
 * Interface.
 */
public interface Controller {
    /**
     * Put collar.
     * @param dog object.
     */
    void putCollar(Dog dog);

    /**
     * Put leash.
     * @param dog object.
     */
    void putLeash(Dog dog);

    /**
     * Feed dog.
     * @param dog object.
     * @param food info about food.
     * @throws DogIsHungryException - exception.
     */
    void feed(Dog dog, double food) throws DogIsHungryException;

    /**
     * Walk.
     * @param dog - object.
     * @throws DogIsNotReadyException - exception.
     */
    void walk(Dog dog) throws DogIsNotReadyException;

    /**
     * Write to file.
     * @param dog - object
     * @param food - info about food.
     * @throws FileNotFoundException - exception.
     */
    void writeToFile(Dog dog, double food) throws FileNotFoundException;
}
