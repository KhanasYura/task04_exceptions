package com.khanas.view;



import com.khanas.controller.ControllerImple;
import com.khanas.model.Dog;

import java.io.IOException;
import java.util.Scanner;

/**
 * View.
 */
public class MyView {

    /**
     * Scanner.
     */
    private Scanner sc = new Scanner(System.in);

    /**
     * Constructor.
     */
    public MyView() {
        double food;
        System.out.println("Вигулювання собак");
        System.out.println("Скільки собак:");
        int dogNumber = sc.nextInt();

        try {
            if (dogNumber <= 0) {
                throw new IOException("Ви ввели непраавильне значення");
            }
        } catch (IOException e) {
            System.out.println("Будьте обережні. " + e);
        }

        System.out.println("Скільки є корму");
        food = sc.nextDouble();
        sc.nextLine();

        for (int i = 0; i < dogNumber; i++) {
            try (ControllerImple controller = new ControllerImple();) {
                System.out.println("Ім`я собаки: ");
                String name = sc.nextLine();
                Dog dog = new Dog(name);

                System.out.println("Надягнути ошийник?(y,n)");
                String select = sc.nextLine();

                if (!select.equals("n") && !select.equals("y")) {
                    throw new IOException();
                }

                if (select.equals("y")) {
                    controller.putCollar(dog);
                }

                System.out.println("Надягнути повідок?(y,n)");
                select = sc.nextLine();

                if (!select.equals("n") && !select.equals("y")) {
                    throw new IOException();
                }

                if (select.equals("y")) {
                    controller.putLeash(dog);
                }

                controller.writeToFile(dog, food);
                food -= 1;
            } catch (IOException e) {
                System.out.println("Wrong input");
            }
        }
    }
}

