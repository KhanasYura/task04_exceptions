package com.khanas;

import com.khanas.view.MyView;

/**
 * Start of the program.
 */
public final class Application {
    /**
     * Constructor.
     */
    private Application() {
    }

    /**
     * main function.
     *
     * @param args - default param.
     */
    public static void main(final String[] args) {
        new MyView();
    }
}
