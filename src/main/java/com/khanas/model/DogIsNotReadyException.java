package com.khanas.model;

/**
 * Exception.
 * If dog is not ready for the walk.
 */
public class DogIsNotReadyException extends Exception {
    /**
     * @param info exception information.
     */
    public DogIsNotReadyException(final String info) {

        System.out.println(info);
    }
}
