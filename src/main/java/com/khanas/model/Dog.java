package com.khanas.model;

import java.time.LocalDateTime;

/**
 * Class dog.
 */
public class Dog {

    /**
     * name - name of the dog.
     */
    private String name;

    /**
     * Collar is on or not.
     */
    private boolean isCollarPutOn = false;

    /**
     * Leash is on or not.
     */
    private boolean isLeashPutOn = false;

    /**
     * dog is hungry or not.
     */
    private boolean isHungry = false;

    /**
     * Start of walking.
     */
    private LocalDateTime walkStart;

    /**
     * Constructor.
     * @param dogName - name of the dog.
     */
    public Dog(final String dogName) {
        this.name = dogName;
    }

    /**
     * @return info if dog is hungry.
     */
    public final boolean isHungry() {
        return isHungry;
    }

    /**
     * @param hungry1 info if dog is hungry.
     */
    public final void setHungry(final boolean hungry1) {
        this.isHungry = hungry1;
    }

    /**
     * @return name.
     */
    public final String getName() {
        return name;
    }

    /**
     * @param name1 - name of the dog.
     */
    public final void setName(final String name1) {
        this.name = name;
    }

    /**
     * @return info if collar is on.
     */
    public final boolean isCollarPutOn() {
        return isCollarPutOn;
    }

    /**
     * @param collarPutOn1 info about collar.
     */
    public final void setCollarPutOn(final boolean collarPutOn1) {
        isCollarPutOn = collarPutOn1;
    }

    /**
     * @return info if leash is on.
     */
    public final boolean isLeashPutOn() {
        return isLeashPutOn;
    }

    /**
     * @param leashPutOn1 info about leash.
     */
    public final void setLeashPutOn(final boolean leashPutOn1) {
        isLeashPutOn = leashPutOn1;
    }

    /**
     * @return info when walk is started.
     */
    public final LocalDateTime getWalkStart() {
        return walkStart;
    }

    /**
     * @param walkStart1 - start of the walk.
     */
    public final void setWalkStart(final LocalDateTime walkStart1) {
        this.walkStart = walkStart1;
    }
}
