package com.khanas.model;

/**
 * Exception.
 * If dog is hungry.
 */
public class DogIsHungryException extends Exception {
    /**
     * @param info exception information.
     */
    public DogIsHungryException(final String info) {

        System.out.println(info);
    }
}
